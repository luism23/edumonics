# Template for Nextjs

It is a template for Nextjs, for used in Startscoinc, used NextJs and Sass

# Install

Open terminal and clone repository [https://gitlab.com/franciscoblancojn/templatenextjs.git](https://gitlab.com/franciscoblancojn/templatenextjs.git)

```bash
git clone https://gitlab.com/franciscoblancojn/templatenextjs.git 
```

Rename Forlder

```bash
mv templatenextjs nameNewProyect
```

Move in Forlder

```bash
cd nameNewProyect
```

Reset git
```bash
rm -fr .git
git init
```

Add your remote repositoy Git

```bash
git remote add origin <server>
```

Add your commit

```bash
git add .
git commit -m "Initial Commit"
```

Push in your Repository

```bash
git push
```

Install dependencies

```bash
npm i
```

Run dev

```bash
npm run dev
```

# Structure

## api
It is forder for connect nextjs with your api

## components
It is forder for create your components for proyect

## data
It is forder for save information static for pages

## functions
It is forder for create your funcions for proyect

## pages
It is forder for create your pages for proyect

## server
It is forder for create your getServerSideProps and getStaticProps for proyect

## Styles
It is forder for save your style and style static for proyect

### Responsive
| Ruel          | Media                 |
| ---           | ---                   |
| sm            | (min-width: 575px)    |
| md            | (min-width: 768px)    |
| lg            | (min-width: 992px)    |
| xl            | (min-width: 1200px)   |

### Font Example Use
| Property Css  | Component Jsx                                     | Value Css                 | Style                 |
| ---           | ---                                               | ---                       | ---                   |
| font-size     | ```<h1 className="font-20">Text</h1>```           | (20px / 16px * 1rem)      | font-size:1.25rem     |
| font-weight   | ```<h1 className="font-w-700">Text</h1>```        | 700                       | font-weight:700       |
| line-height   | ```<h1 className="line-h-15">Text</h1>```         | (15/10)                   | line-height:1.5       |

#### Font Responsive Example Use
| Property Css  | Component Jsx                                     |
| ---           | ---                                               |
| font-size     | ```<h1 className="font-sm-20">Text</h1>```        |
| font-weight   | ```<h1 className="font-md-w-700">Text</h1>```     |
| line-height   | ```<h1 className="line-h-lg-15">Text</h1>```      | 

### Heght Example Use
| Property Css  | Component Jsx                                     | Value Css                 | Style                 |
| ---           | ---                                               |---                        | ---                   |
| height        | ```<h1 className="height-20">Text</h1>```         | (20px / 16px * 1rem)      | height:1.25rem        |
| height        | ```<h1 className="height-vh-20">Text</h1>```      | 20vh                      | height:20vh           |
| height        | ```<h1 className="height-p-20">Text</h1>```       | 20%                       | height:20%            |

#### Heght Responsive Example Use
| Property Css  | Component Jsx                                     |
| ---           | ---                                               |
| height        | ```<h1 className="height-sm-20">Text</h1>```      |
| height        | ```<h1 className="height-xl-vh-20">Text</h1>```   |
| height        | ```<h1 className="height-md-p-20">Text</h1>```    |

### Width Example Use
| Property Css  | Component Jsx                                     | Value Css                 | Style                 |
| ---           | ---                                               |   ---                     | ---                   |
| width         | ```<h1 className="width-20">Text</h1>```          | (20px / 16px * 1rem)      | width:1.25rem         |
| width         | ```<h1 className="width-vh-20">Text</h1>```       | 20vh                      | width:20vh            |
| width         | ```<h1 className="width-p-20">Text</h1>```        | 20%                       | width:20%             |

#### Width Responsive Example Use
| Property Css  | Component Jsx                                     |
| ---           | ---                                               |
| width         | ```<h1 className="width-sm-20">Text</h1>```       |
| width         | ```<h1 className="width-xl-vh-20">Text</h1>```    |
| width         | ```<h1 className="width-md-p-20">Text</h1>```     |

### Margin Example Use
| Property Css  | Component Jsx                                     | Value Css                 | Style                 |
| ---           | ---                                               |   ---                     | ---                   |
| margin        | ```<h1 className="m-20">Text</h1>```              | (20px / 16px * 1rem)      | margin:1.25rem        |
| margin        | ```<h1 className="m-auto">Text</h1>```            | auto                      | margin:auto           |
| margin-top    | ```<h1 className="m-t-20">Text</h1>```            | (20px / 16px * 1rem)      | margin-top:1.25rem    |
| margin-top    | ```<h1 className="m-t-auto">Text</h1>```          | auto                      | margin-top:auto       |
| margin-left   | ```<h1 className="m-l-20">Text</h1>```            | (20px / 16px * 1rem)      | margin-left:1.25rem   |
| margin-right  | ```<h1 className="m-r-17">Text</h1>```            | (17px / 16px * 1rem)      | margin-right:1.0625rem|
| margin-bottom | ```<h1 className="m-b-22">Text</h1>```            | (22px / 16px * 1rem)      | margin-bottom:1.375rem|
| margin x      | ```<h1 className="m-h-25">Text</h1>```            | (25px / 16px * 1rem)      | margin-left:1.5625rem; <br/> margin-right:1.5625rem;   |
| margin y      | ```<h1 className="m-v-24">Text</h1>```            | (24px / 16px * 1rem)      | margin-top:1.5rem; <br/> margin-bottom:1.5rem;   |

#### Margin Responsive Example Use
| Property Css  | Component Jsx                                     |
| ---           | ---                                               |
| margin        | ```<h1 className="m-sm-20">Text</h1>```           |
| margin-top    | ```<h1 className="m-lg-t-auto">Text</h1>```       |
| margin-left   | ```<h1 className="m-xl-l-20">Text</h1>```         |
| margin-right  | ```<h1 className="m-md-r-17">Text</h1>```         |
| margin-bottom | ```<h1 className="m-sm-b-22">Text</h1>```         |
| margin x      | ```<h1 className="m-lg-h-25">Text</h1>```         |
| margin y      | ```<h1 className="m-xl-v-24">Text</h1>```         |

### Padding Example Use
| Property Css   | Component Jsx                                    | Value Css                 | Style                  |
| ---            | ---                                              |   ---                     | ---                    |
| padding        | ```<h1 className="p-20">Text</h1>```             | (20px / 16px * 1rem)      | padding:1.25rem        |
| padding        | ```<h1 className="p-auto">Text</h1>```           | auto                      | padding:auto           |
| padding-top    | ```<h1 className="p-t-20">Text</h1>```           | (20px / 16px * 1rem)      | padding-top:1.25rem    |
| padding-top    | ```<h1 className="p-t-auto">Text</h1>```         | auto                      | padding-top:auto       |
| padding-left   | ```<h1 className="p-l-20">Text</h1>```           | (20px / 16px * 1rem)      | padding-left:1.25rem   |
| padding-right  | ```<h1 className="p-r-17">Text</h1>```           | (17px / 16px * 1rem)      | padding-right:1.0625rem|
| padding-bottom | ```<h1 className="p-b-22">Text</h1>```           | (22px / 16px * 1rem)      | padding-bottom:1.375rem|
| padding x      | ```<h1 className="p-h-25">Text</h1>```           | (25px / 16px * 1rem)      | padding-left:1.5625rem; <br/> padding-right:1.5625rem;   |
| padding y      | ```<h1 className="p-v-24">Text</h1>```           | (24px / 16px * 1rem)      | padding-top:1.5rem; <br/> padding-bottom:1.5rem;   |

#### Padding Responsive Example Use
| Property Css   | Component Jsx                                    |
| ---            | ---                                              |
| padding        | ```<h1 className="p-sp-20">Text</h1>```          |
| padding-top    | ```<h1 className="p-lg-t-auto">Text</h1>```      |
| padding-left   | ```<h1 className="p-xl-l-20">Text</h1>```        |
| padding-right  | ```<h1 className="p-md-r-17">Text</h1>```        |
| padding-bottom | ```<h1 className="p-sm-b-22">Text</h1>```        |
| padding x      | ```<h1 className="p-lg-h-25">Text</h1>```        |
| padding y      | ```<h1 className="p-xl-v-24">Text</h1>```        |

### Position Example Use
| Property Css  | Component Jsx                                     | Value Css                 | Style                 |
| ---           | ---                                               |   ---                     | ---                   |
| position      | ```<h1 className="pos-r">Text</h1>```             | relative                  | position:relative     |
| position      | ```<h1 className="pos-a">Text</h1>```             | absolute                  | position:absolute     |
| position      | ```<h1 className="pos-f">Text</h1>```             | fixed                     | position:fixed        |
| position      | ```<h1 className="pos-s">Text</h1>```             | static                    | position:static       |

#### Position Responsive Example Use
| Property Css  | Component Jsx                                     |
| ---           | ---                                               |
| position      | ```<h1 className="pos-sm-r">Text</h1>```          |
| position      | ```<h1 className="pos-md-a">Text</h1>```          |
| position      | ```<h1 className="pos-lg-f">Text</h1>```          |
| position      | ```<h1 className="pos-xl-s">Text</h1>```          |

### Text Example Use
| Property Css      | Component Jsx                                                 | Value Css                 | Style                         |
| ---               | ---                                                           |   ---                     | ---                           |
| text-align        | ```<h1 className="text-left">Text</h1>```                     | left                      | text-align:left               |
| text-align        | ```<h1 className="text-center">Text</h1>```                   | center                    | text-align:center             |
| text-align        | ```<h1 className="text-right">Text</h1>```                    | right                     | text-align:right              |
| text-decoration   | ```<h1 className="text-decoration-none">Text</h1>```          | text-decoration           | text-decoration:none          |
| text-decoration   | ```<h1 className="text-decoration-through">Text</h1>```       | text-decoration           | text-decoration:line-through  |
| text-decoration   | ```<h1 className="text-decoration-underline">Text</h1>```     | text-decoration           | text-decoration:underline     |
| text-decoration   | ```<h1 className="text-decoration-overline">Text</h1>```      | text-decoration           | text-decoration:overline      |

#### Text Responsive Example Use
| Property Css      | Component Jsx                                                 |
| ---               | ---                                                           |
| text-align        | ```<h1 className="text-md-left">Text</h1>```                  |
| text-align        | ```<h1 className="text-sm-center">Text</h1>```                |
| text-align        | ```<h1 className="text-lg-right">Text</h1>```                 |
| text-decoration   | ```<h1 className="text-xl-decoration-none">Text</h1>```       |
| text-decoration   | ```<h1 className="text-md-decoration-through">Text</h1>```    |
| text-decoration   | ```<h1 className="text-sm-decoration-underline">Text</h1>```  |
| text-decoration   | ```<h1 className="text-lg-decoration-overline">Text</h1>```   |

### Top Example Use
| Property Css      | Component Jsx                                                 | Value Css                 | Style                         |
| ---               | ---                                                           |   ---                     | ---                           |
| top               | ```<h1 className="top-40">Text</h1>```                        | (40px / 16px * 1rem)      | top: 2.5rem                   |
| top               | ```<h1 className="top-p-40">Text</h1>```                      | 40%                       | top: 40%                      |

#### Top Responsive Example Use
| Property Css      | Component Jsx                                                 |
| ---               | ---                                                           |
| top               | ```<h1 className="top-md-40">Text</h1>```                     |
| top               | ```<h1 className="top-lg-p-40">Text</h1>```                   |

### Left Example Use
| Property Css      | Component Jsx                                                 | Value Css                 | Style                         |
| ---               | ---                                                           |   ---                     | ---                           |
| left              | ```<h1 className="left-40">Text</h1>```                       | (40px / 16px * 1rem)      | left: 2.5rem                  |
| left              | ```<h1 className="left-p-40">Text</h1>```                     | 40%                       | left: 40%                     |

#### Left Responsive Example Use
| Property Css      | Component Jsx                                                 |
| ---               | ---                                                           |
| left              | ```<h1 className="left-md-40">Text</h1>```                    |
| left              | ```<h1 className="left-lg-p-40">Text</h1>```                  |

### Right Example Use
| Property Css      | Component Jsx                                                 | Value Css                 | Style                         |
| ---               | ---                                                           |   ---                     | ---                           |
| right             | ```<h1 className="right-40">Text</h1>```                      | (40px / 16px * 1rem)      | right: 2.5rem                 |
| right             | ```<h1 className="right-p-40">Text</h1>```                    | 40%                       | right: 40%                    |

#### Right Responsive Example Use
| Property Css      | Component Jsx                                                 |
| ---               | ---                                                           |
| right             | ```<h1 className="right-md-40">Text</h1>```                   |
| right             | ```<h1 className="right-lg-p-40">Text</h1>```                 |

### Bottom Example Use
| Property Css      | Component Jsx                                                 | Value Css                 | Style                         |
| ---               | ---                                                           |   ---                     | ---                           |
| bottom            | ```<h1 className="bottom-40">Text</h1>```                     | (40px / 16px * 1rem)      | bottom: 2.5rem                |
| bottom            | ```<h1 className="bottom-p-40">Text</h1>```                   | 40%                       | bottom: 40%                   |

#### Bottom Responsive Example Use
| Property Css      | Component Jsx                                                 |
| ---               | ---                                                           |
| bottom            | ```<h1 className="bottom-md-40">Text</h1>```                  |
| bottom            | ```<h1 className="bottom-lg-p-40">Text</h1>```                |

### z-index Example Use
| Property Css      | Component Jsx                                                 | Value Css                 | Style                         |
| ---               | ---                                                           |   ---                     | ---                           |
| z-index           | ```<h1 className="z-index-5">Text</h1>```                     | 5                         | z-index: 5                    |
| z-index           | ```<h1 className="z-index--5">Text</h1>```                    | -5                        | z-index: -5                   |

#### z-index Responsive Example Use
| Property Css      | Component Jsx                                                 |
| ---               | ---                                                           |
| z-index           | ```<h1 className="z-index-md-5">Text</h1>```                  |
| z-index           | ```<h1 className="z-index-lg--5">Text</h1>```                 |

### Display Example Use
| Property Css      | Component Jsx                                                 | Value Css                 | Style                             |
| ---               | ---                                                           |   ---                     | ---                               |
| display           | ```<h1 className="d-flex">Text</h1>```                        | flex !important;          | display: flex !important;         |
| display           | ```<h1 className="d-inline-block">Text</h1>```                | inline-block !important;  | display: inline-block !important; |
| display           | ```<h1 className="d-block">Text</h1>```                       | block !important;         | display: block !important;        |
| display           | ```<h1 className="d-none">Text</h1>```                        | none !important;          | display: none !important;         |

#### Display Responsive Example Use
| Property Css      | Component Jsx                                                 |
| ---               | ---                                                           |
| display           | ```<h1 className="d-md-flex">Text</h1>```                        |
| display           | ```<h1 className="d-sm-inline-block">Text</h1>```                |
| display           | ```<h1 className="d-xl-block">Text</h1>```                       |
| display           | ```<h1 className="d-lg-none">Text</h1>```                        |

## svg
It is forder for save your svg for proyect


## Developer

[Francisco Blanco](https://franciscoblanco.vercel.app/)