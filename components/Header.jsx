import Link from "next/link"
import DATA from "@/data/components/Header"

import Data from "@/data/components/Header"
import { useEffect, useRef, useState } from "react"
import Menu from "@/svg/menu"


const LinkTitle = ({ title = "", link = "" }) => {
    return (
        <>
            <div className="">
                <Link href={link}>
                    <a className="font-16 font-montserrat font-w-500  color-gray m-r-30">
                        {title}
                    </a>
                </Link>
            </div>
        </>
    )
}



const Index = ({ img = "", linkTitle = [] }) => {
    const header = useRef(null)
    const [heightMenu, setHeightMenu] = useState(0)
    useEffect(() => {
        const height = header.current.offsetHeight
        setHeightMenu(height)
    }, [])
    return (
        <>
            <header ref={header} className="flex z-index-9 header bg-white-2 pos-f top-0 left-0 width-p-100 height-90 p-v-15">
                <div className="container flex-align-center flex p-h-15 flex-gap-30 flex-justify-between ">
                    <div className="flex-6 flex">
                        <img className="flex width-150 " src={`/image/${img}`} alt="" />
                    </div>
                    <div className="border-radius-50 flex-6 d-lg-none flex flex-justify-right">
                        <button
                            className="color-white bg-transparent border-radius-20 border-0"
                            onClick={() => {
                                document.body.classList.toggle("activeMenu")
                            }}>
                            <Menu size={34} className="border-radius-50 color-yellow-1" />
                        </button>
                    </div>
                    <div
                        className="menuMovil bg-white-2 flex-12 flex-lg-7 flex"
                        style={{ "--heightMenu": heightMenu + "px" }}
                        onClick={(e) => {
                            if (e.target.tagName == "A") {
                                document.body.classList.toggle("activeMenu")
                            }
                        }}>
                        <div className="flex menuMovil-2">
                            {
                                linkTitle.map((e, i) => {
                                    return (
                                        <LinkTitle
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}
export default () => (<Index {...DATA} />)