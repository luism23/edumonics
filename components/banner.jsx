import Link from "next/link"

import Img from "@/components/Img"

const LinkInput = ({ link = "", text = "" ,color=""}) => {
    return (
        <>
            <div className="flex ">
                <Link href={link}>
                    <a className="width-230 bg-yellow-1 flex flex-justify-center font-montserrat font-18 font-w-500  border-radius-10 flex   p-v-20 p-h-29 color-black">
                        {text}
                    </a>

                </Link>
            </div>
        </>
    )
}


const Index = ({ img = "", title = "",  linkInput = [] }) => {
    return (
        <>
            <div className="flex ">
                <div className="flex container p-h-15 flex-md-gap-20 flex-align-center m-t-200 m-md-t-128 m-b-128">
                    <div className="flex-12 flex-md-6 flex-md-gap-20 flex-justify-center flex">
                        <h1 className="flex font-w-700 font-montserrat text-center font-60 color-black m-b-40 ">
                            {title}
                        </h1>
                        <div className="flex">
                            {
                                linkInput.map((e, i) => {
                                    return (
                                        <LinkInput
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                    </div>
                    <div className="flex-12 flex-md-6 flex-md-gap-20 flex flex-justify-center">
                        <Img
                            src={img}
                            
                            classNameImg="width-max-700 width-md-p-max-100 m-t-50 m-md-t-0 border-radius-50"
                        />
                    </div>
                </div>
            </div>

        </>
    );
};
export default Index;
