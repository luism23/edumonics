import Link from "next/link"


const Index = ({ title="", textInfo = "", link = "", textLink = "" }) => {
    return (
        <>
            <div className="container p-h-15">
                <div className=" flex-justify-center flex">
                    <h2 className="color-black font-montserrat font-w-700 font-45 m-t-100 m-b-20">
                        {title}
                    </h2>
                </div>
                <div className="flex-12 flex-sm-6 bg-yellow-1 border-radius-10">
                    <div className="p-h-15 p-lg-h-96 p-t-96 p-b-48">

                        <p className="m-b-40 color-white font-w-600 font-montserrat font-50 ">{textInfo}</p>

                        <div className="m-b-39">
                            <form className="">
                                <input type="text" name="fname" placeholder="Your Name" className="width-p-100 whidth-432 border-radius-8  border-style-solid border-1 bg-blue-1 login-input color-gray font-18 font-montserrat p-v-18 p-l-32 m-b-16 boder-white-2" />
                                <input type="email" name="lname" placeholder="Email" className=" width-p-100 border-radius-8 bg-blue-1 border-style-solid border-2 boder-white-2 login-input color-gray font-18  m-b-16 font-montserrat p-v-18 p-l-32" />
                                <input type="text" name="lname" placeholder="Direccion" className="width-p-100 m-b-16 border-radius-8 bg-blue-1 border-style-solid border-1 login-input color-gray font-18 font-montserrat p-v-18 p-l-32 boder-white-2" />

                            </form>
                        </div>
                        <div className="heigth-30 p-v-5 text-center bg-green-1 border-radius-10">
                            <Link href={link}>
                                <a className="  font-montserrat font-20 color-blue-1 bg-blue-1 color-white font-w-500  ">{textLink}</a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index

