
const Seccion = ({img="",title="",text=""}) => {
    return (
        <>
            <div className="seccion flex flex-justify-between flex-md-gap-50 ">
                <div className="flex-12 flex-md-6 flex-md-gap-50 m-v-130 m-b-96 text-center ">
                    <h2 className="m-b-16 color-blue-1 font-montserrat font-w-700 font-35">
                        {title}
                    </h2>
                    <p className=" color-blue-1 font-montserrat font-18">
                        {text}
                    </p>
                </div>
                <div className="flex-12 flex-md-6 flex-md-gap-50">
                    <img className="flex " src={`/image/${img}`} alt="" />
                </div>
            </div>
        </>
    )
}


const Index = ({seccion=[]}) => {
    return (
        <>
            <div className="container p-h-15 m-b-50 m-md-b-0 ">
                {
                    seccion.map((e, i) => {
                        return (
                            <Seccion
                                key={i}
                                {...e}
                            />
                        )
                    })
                }
            </div>
        </>
    )
}
export default Index