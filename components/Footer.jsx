import Link from "next/link"
import DATA from "@/data/components/Footer"

const Links = ({ link = "", linkText = "" }) => {
    return (
        <>
            <div className=" m-b-10">
                <Link href={link}>
                    <a className="font-30  font-baloo2 color-white   ">{linkText}</a>
                </Link>
            </div>
        </>
    )
}


const LinkTitle = ({ title = "", links = [] }) => {
    return (
        <>
            <div className="m-r-150">
                <div>
                    <h1 className=" ">
                        {title}
                    </h1>
                </div>
                <div>
                    {
                        links.map((e, i) => {
                            return (
                                <Links
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }

                </div>
            </div>
        </>
    )
}

const Index = ({ textSpan = "", subText = "", text = "", img = "", linkTitle = [] }) => {
    return (
        <>
            <footer className="p-v-20 bg-gray m-t-128">
                <div className="container p-h-15 flex-md-gap-40 flex flex-justify-between m-t-70">
                    <div className="flex-12 flex-md-3 flex-md-gap-40 text-center m-b-20">
                        <div>
                            <img className="flex " src={`/image/${img}`} alt="" />
                        </div>
                        <div>
                            <h3 className="color-white font-15 font-montserrat">
                                {subText}
                            </h3>
                            <h2 className="color-white font-18 font-montserrat">
                                {text}
                            </h2>
                        </div>
                    </div>
                    <div className="flex-12 flex-md-9 flex-md-gap-40 ">
                        <div className="flex flex-justify-center">
                            {
                                linkTitle.map((e, i) => {
                                    return (
                                        <LinkTitle
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }

                        </div>
                    </div>
                </div>
                <div className="flex-justify-center">
                    <div className="text-center m-t-80">
                        <span className="color-white font-20 font-montserrat ">
                            {textSpan}
                        </span>
                    </div>
                </div>
            </footer>
        </>
    )
}
export default () => (<Index {...DATA} />)