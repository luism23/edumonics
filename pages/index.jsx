import Content from "@/components/Content"
import Banner from "@/components/banner"
import ContentInfo from "@/components/contentInfo"
import Register from "@/components/register"

import Info from "@/data/pages/index"

const Index = () => {
    return (
        <>
            <Content>
                <Banner {...Info.banner}/>
                <ContentInfo {...Info.contentInfo}/>
                <Register {...Info.register}/>

            </Content>
        </>
    )
}
export default Index