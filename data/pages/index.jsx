export default {
    banner: {
        title: "Make your Children Future-Ready with Mnemonics based learnings.",
        img: "banner.png",
        linkInput: [
            {
                link: "/",
                text: "Get Started"
            },
        ]
    },
    contentInfo: {
        seccion: [
            {
                title: "Stop Cramming!",
                text: "as we can’t retain what we cram.",
                img: "img1.png",
            },
            {
                title: "Enjoy Learning as never before!",
                text: "with our AI powered Mnemonics based Learning Solution",
                img: "img2.png",
            },
            {
                title: "And give more time to your Leisure Acitvities.",
                img: "img3.png",
            },
            {
                title: "Provide us the information in the form of texts, images, audios and pdfs.",
                img: "img4.png",
            },
            {
                title: "Our Algorithm will convert your information into sequence of relevant images[Mnemonics] that will make your learning task super easy!",
                img: "img5.png",
            },
            {
                title: "Easy Learning saves your Time that you can invest in your Leisure Activities.",
                img: "img6.png",
            },
        ]
    },
    register: {
        title: "Register",
        textInfo: "Don’t think more. Register now to avail 3 Days FREE trial for Edumonics Course.",
        link: "/",
        textLink: "Register Now>>"

    }
}